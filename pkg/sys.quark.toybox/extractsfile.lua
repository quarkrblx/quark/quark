local exports = {}
exports.PackageInfo = {}
exports.StoreInfo = {}

-- Package Information --

--[[
	The PackageName is used to quickly
	identify the author and runlevel.
	This also specifies the actual
	name of the package.
	
	Format
	[runlevel].[author].[parents...].[name]
]]--
exports.PackageInfo.PackageName = "sys.quark.toybox"
--[[
	The alias should be the name of the
	package (everything after the last
	dot.)
	
	If you wrote your package name
	in PascalCase or snake_case, please
	write it in camelCase to comply with
	standards.
]]--
exports.PackageInfo.Alias = "toybox"
--[[
	Specify the type of the package.
	Dictates what the package is.
	
	Type reference:
	- Engine space -
	
	EngineExtension - A extension that runs
	in only engine space. EngineExtension 
	running in the engine will be able to 
	cause panics by throwing an uncaught 
	exception.
	
	EngineExceptionBundle - Same as
	EngineException but contains multiple 
	extensions.
	
	EngineLibraries - Library(s) that can
	be used in only engine space.
	
	ProtectedServices - Services that can be
	controlled only by the engine. Recievers
	can be used to pick up the service but
	controlling the service requires a
	special ProtectedController. 
	
	ProtectedControllers - Controllers
	for protected services.
	
	EngineRuntime - Runtimes that can only
	run in the engine space.
	
	- Shared space -
	SharedExtension - A extension that run
	in both user space and engine space. 
	SharedExtensions running in the engine 
	will be able to cause panics by throwing
	an uncaught exception.
	
	SharedExtensionBundle - Same as
	SharedExtension but contains multiple
	extensions.
	
	SharedLibraries - Library(s) that can
	be used in both engine space and user
	space. Most libraries should have their
	runlevel here.
	
	SharedRuntime - Runtimes that can run 
	in both the user space and engine
	space.
	
	Services - Services are wrappers that
	allow Recievers to execute functions
	authorized to them by a Controller.
	(simply put, alternative to RemoteEvents)
	- User space -
	UserExtension - A extension that run
	in userspace.
	
	UserExtensionBundle - Same as UserExtension
	but contains multiple extensions.
	
	UserLibraries - Library(s) that can
	be used in only user space.
	
	UserRuntimes - Runtimes that can only
	run in the user space.
	
	Recievers - Modules on the recieving 
	end of the service.
	
	Controllers - Modules on the controlling
	end of the service.
	
	IDEExtensions - 
	
	- Note -
	Tables can be specified to include multiple
	types. Each type must have an individual
	folder in the contents folder.
]]--
exports.PackageInfo.Type = "SharedLibraries"
exports.PackageInfo.Version = "${QUARK_VER}"
exports.PackageInfo.Author = "Quark Development Team"
--[[
	Specify the runtime level of the application.
	Dictates where it will execute and what
	permissions it will be able to use.
	
	sys - System runtime level
	
	usr - User runtime level
	
	studio - Studio & IDE only runtime
	level
	
	temp - Temporary runtime level
	
	init - Only meant to run during
	initalizing runtime level.
	
	root - Has control over everything
	runtime level.
	
	console - Console runtime level.
]]--
exports.PackageInfo.Runlevel = "sys"
--[[
	License of the application.
	If needed or required by the
	license, you may put the full license
	text in a seperate LICENSE ModuleScript.
]]--
exports.PackageInfo.License = "BSD"
--[[
	Dependencies:
	
	Add full dependency package names
	(sys.quark.Quark) instead of aliases
	(Quark)
	
	Ion and Quark should usually be in
	the dependencies, Ion can be removed
	if you're working on an extension that:
	
	a. Is written for QuarkAero
	b. Is backend-agnostic (nearly always
	not the case)
--]]

exports.PackageInfo.Dependencies = {
	"sys.quark.Quark"
}

--[[
	Recommended dependencies:
	Dependencies that do not have
	to be installed.
]]--

exports.PackageInfo.Recommended = {}

-- Store Information --

--[[
	The title specifies what it should be called
	when you view it in the store. This differs
	from the PackageName which is used to
	quickly identify the author and runlevel
	([runlevel].[author].[parents...].[alias])
	or the alias which is a way to quickly
	identify the package (for example for
	installation) without typing the full package
	name.
]]--
exports.StoreInfo.Title = "Toybox"
-- Full description of your package that appears
-- on the package page.
exports.StoreInfo.Description = [[Toybox contains all essential commands]]
-- The tagline for your package which is a one
-- line long summary that is shown in the
exports.StoreInfo.Tagline = "All essential commands for Quark." 

-- Misc. --

--[[
	Pre-install MOTD, this appears before the
	user confirms installation.
]]--
exports.MOTD = nil
--[[
	Post-install script. 
	
	Two tables will be passed. The Quark Package
	API, the package filesystem (or just the
	package location if not isolated.) The third
	table is only passed if the package is not
	isolated, and contains the dependencies
	used for the package.
	
	You shouldn't be too worried about package
	isolation because it has to be disabled
	using a flag.
]]--

exports.PostInstall = function(
	Quark,
	Package,
	Dependencies
) => number 
	return 0
end

-- Dictate that this should be read off
-- the Toybox model
exports.onlineModel = 0

return exports